# modbus conda recipe

Home: https://github.com/epics-modules/modbus

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS support for communication with PLCs and other devices via the Modbus protocol over TCP, serial RTU, and serial ASCII links
