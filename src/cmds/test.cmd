require modbus

epicsEnvSet("PLC_NAME", "plc-test")
epicsEnvSet("PLC_IP", "127.0.0.1")

iocshLoad("$(modbus_DIR)/modbus_s7plc.iocsh")

iocInit()

dbl
